package com.example.examplemod;

import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.CauldronBlock;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.SpawnReason;
import net.minecraft.entity.monster.SlimeEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.Hand;
import net.minecraft.util.RegistryKey;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.LightType;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.eventbus.api.Event;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

// The value here should match an entry in the META-INF/mods.toml file
@Mod("examplemod")
public class ExampleMod {
    // Directly reference a log4j logger.
    private static final Logger LOGGER = LogManager.getLogger();

    public ExampleMod() {
        // Register ourselves for server and other game events we are interested in
        MinecraftForge.EVENT_BUS.register(this);
    }

    @SubscribeEvent
    public void onCauldronCraft(PlayerInteractEvent.RightClickBlock event) {
        BlockPos pos = event.getPos();
        World world = event.getWorld();
        BlockState blockState = world.getBlockState(pos);
        if (event.getWorld() instanceof ServerWorld) {

            if (blockState.getBlock() == Blocks.CAULDRON) {
                PlayerEntity player = event.getPlayer();
                Hand hand = event.getHand();
                ItemStack heldItem = player.getHeldItem(hand);
                int skylight = world.getLightFor(LightType.SKY, pos);
                int blocklight = world.getLightFor(LightType.BLOCK, pos);
                float moonFactor = world.getMoonFactor();
                BlockState below = world.getBlockState(pos.down());
                int level = blockState.get(CauldronBlock.LEVEL);
                Biome biome = world.getBiome(pos);
                RegistryKey<World> dimensionKey = world.getDimensionKey();
                boolean canSeeSky = skylight >= 15;

//                LOGGER.debug("===============");
//                LOGGER.debug("Skylight: " + skylight);
//                LOGGER.debug("Blocklight: " + blocklight);
//                LOGGER.debug("Moonfactor: " + moonFactor);
//                LOGGER.debug("Below: " + below);
//                LOGGER.debug("Helditem: " + heldItem);
//                LOGGER.debug("Waterlevel: " + level);

                if (level > 0 && canSeeSky && world.isNightTime() && moonFactor >= 0.9 && below.getBlock() == Blocks.SOUL_FIRE && heldItem.getItem() == Items.MAGMA_CREAM) {
                    CompoundNBT tag = new CompoundNBT();
                    tag.putInt("Size", 0);
                    SlimeEntity slimeEntity = EntityType.SLIME.create((ServerWorld) world, tag, null, player, pos.up(), SpawnReason.MOB_SUMMONED, false, false);
                    if (slimeEntity != null) {
                        world.addEntity(slimeEntity);
                        world.setBlockState(pos, blockState.with(CauldronBlock.LEVEL, level - 1));
                        if (!player.isCreative() && !player.isSpectator()) {
                            heldItem.shrink(1);
                            player.setHeldItem(hand, heldItem);
                        }
                        event.setUseBlock(Event.Result.DENY);
                        event.setUseItem(Event.Result.DENY);
                    }
                }

            }

        }
        else {

            if (blockState.getBlock() == Blocks.CAULDRON) {
                PlayerEntity player = event.getPlayer();
                Hand hand = event.getHand();
                ItemStack heldItem = player.getHeldItem(hand);
                int skylight = world.getLightFor(LightType.SKY, pos);
                int blocklight = world.getLightFor(LightType.BLOCK, pos);
                float moonFactor = world.getMoonFactor();
                BlockState below = world.getBlockState(pos.down());
                int level = blockState.get(CauldronBlock.LEVEL);
                Biome biome = world.getBiome(pos);
                RegistryKey<World> dimensionKey = world.getDimensionKey();
                boolean canSeeSky = skylight >= 15;
                LOGGER.debug("s");
                LOGGER.debug(level > 0);
                LOGGER.debug(canSeeSky);
//                LOGGER.debug(world.isNightTime());
                LOGGER.debug(moonFactor >= 0.9);
                LOGGER.debug(below.getBlock() == Blocks.SOUL_FIRE);
                LOGGER.debug(heldItem.getItem() == Items.MAGMA_CREAM);

                if (level > 0 && canSeeSky && moonFactor >= 0.9 && below.getBlock() == Blocks.SOUL_FIRE && heldItem.getItem() == Items.MAGMA_CREAM) {
                    LOGGER.debug("TRUE");
                    event.getPlayer().playSound(SoundEvents.PARTICLE_SOUL_ESCAPE, 1F, 1F);
                }
            }
        }
    }
}
